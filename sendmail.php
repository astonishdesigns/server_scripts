#!/usr/bin/php
################################################################################
# Capture and log emails instead of sending them
#
# 1. Create a folder on the server: /var/log/phpmail
# 2. Set the owner and group of the folder: serverUser:apache
# 3. Permissions: 775
# 4. Configure php.ini to call this script:
#        sendmail_path = /usr/local/astonish/server_scripts/sendmail.php
################################################################################
<?php
$input = file_get_contents('php://stdin');
preg_match('|^To: (.*)|', $input, $matches);
$t = tempnam("/var/log/phpmail", $matches[1]);
file_put_contents($t, $input);
