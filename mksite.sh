#!/bin/bash

###############################################################################
# Make Site
#
# Given a site name, create the docroot and vhost.
# Run this script as sudo.
###############################################################################

if [[ -z $1 || -z $2 || -z $3 ]]; then
  echo "ERROR: Missing arguments"
  echo ""
  echo "Usage: mksite [site domain] [owner] [group]"
  echo "       mksite www.mysite.com user apache"
  exit 1
fi

SITE=$1
OWNER=$2
GROUP=$3
DOCROOT="/var/www/$SITE"
BACKUP_DIR="/var/backup/$SITE"

###############################################################################
# Create folders
#
# /var/www/[site]
# /var/www/[site]/builds
# /var/www/[site]/logs
# /var/www/[site]/shared
# /var/www/[site]/shared/files
# /var/backup/[site]
###############################################################################

if [[ ! -d $DOCROOT ]]; then
  echo "=== Creating docroot ($DOCROOT) ==="
  mkdir $DOCROOT
else
  echo "NOTICE: Docroot ($DOCROOT) already exists. Skipping..."
fi

if [[ ! -d $DOCROOT/builds ]]; then
  echo "=== Creating builds folder ==="
  mkdir $DOCROOT/builds
else
  echo "NOTICE: Builds ($DOCROOT/builds) already exists. Skipping..."
fi

if [[ ! -d $DOCROOT/logs ]]; then
  echo "=== Creating logs folder ==="
  mkdir $DOCROOT/logs
else
  echo "NOTICE: Logs ($DOCROOT/logs) already exists. Skipping..."
fi

if [[ ! -d $DOCROOT/shared ]]; then
  echo "=== Creating shared folder ==="
  mkdir $DOCROOT/shared
else
  echo "NOTICE: Shared ($DOCROOT/shared) already exists. Skipping..."
fi

if [[ ! -d $DOCROOT/shared/files ]]; then
  echo "=== Creating files folder ==="
  mkdir $DOCROOT/shared/files
else
  echo "NOTICE: Files ($DOCROOT/shared/files) already exists. Skipping..."
fi

echo "=== Settings permissions ==="
chown -R $OWNER:$GROUP $DOCROOT

if [[ ! -d $BACKUP_DIR ]]; then
  echo "=== Creating backup folder ==="
  mkdir $BACKUP_DIR
  chown -R $OWNER $BACKUP_DIR
else
  echo "NOTICE: Backup folder ($BACKUP_DIR) already exists. Skipping..."
fi


###############################################################################
# Create vhost
###############################################################################

if [[ ! -f /etc/httpd/conf.d/${SITE}.conf ]]; then
  echo "=== Configuring Virtual Host==="
  cat  > /etc/httpd/conf.d/${SITE}.conf <<eof
    <VirtualHost *:80>
        ServerName $SITE
        DocumentRoot /var/www/$SITE/htdocs
        <Directory /var/www/$SITE/htdocs>
          Options All
          Allow from all
          AllowOverride All
        </Directory>
        ErrorLog "|/usr/sbin/cronolog /var/www/$SITE/logs/%Y/%m/%Y%m%d-error.log"
        CustomLog "|/usr/sbin/cronolog /var/www/$SITE/logs/%Y/%m/%Y%m%d-access.log" combined

        <FilesMatch "\.(js|css|png|jpg|gif|ico)$">
        FileETag None
        </FilesMatch>
        AddOutputFilterByType DEFLATE text/css application/javascript application/x-javascript text/html
        AddDefaultCharset utf-8
        Header add Vary "Accept-Encoding"
        Header add X-WebNode "%{HOSTNAME}e"
    </VirtualHost>
eof

  echo "=== Reloading Apache Configuration ==="
  service httpd reload

else
  echo "NOTICE: Vhost already exists.  Skiping..."
fi





